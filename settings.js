module.exports = {
  internal: {
    documentupdater: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
  apis: {
    project_history: {
      enabled: !!process.env.PROJECT_HISTORY_HOST,
    },
  },
};
